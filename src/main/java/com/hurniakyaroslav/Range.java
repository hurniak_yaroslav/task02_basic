package com.hurniakyaroslav;

import java.util.Scanner;

/**
 * Class Range describes the range, the limits of which were entered by user.
 */
public class Range {
    /**
     * Start limit of range.
     */
    private int startRange;
    /**
     * End limit of range.
     */
    private int endRange;
    /**
     * max odd element of sequance.
     */
    private int maxOdd = 0;
    /**
     * max even element of sequance.
     */
    private int maxEven = 0;

    /**
     * Constructor of Range, which contains two limits: startRange and endRange.
     */
    Range() {
        startRange = 0;
        endRange = 0;
    }

    /**
     *
     * @param extStartRange The start limit of range
     * @param extEndRange The end limit of range
     */
    Range(final int extStartRange, final int extEndRange) {
        this.startRange = extStartRange;
        this.endRange = extEndRange;
        checkMaxValues();
    }

//    public int getStartRange() {
//        return startRange;
//    }
//
//    public int getEndRange() {
//        return endRange;
//    }

    /**
     * User enters range limits, then method checks max odd
     * and even values in range with method checkMaxValues.
     */
    public final void initRange() {
        System.out.println("Enter range limits: ");
        Scanner scanner = new Scanner(System.in);
        startRange = scanner.nextInt();
        endRange = scanner.nextInt();
        System.out.print("Enterred range: " + '[' + startRange
                + ',' + endRange + "]\n");
        checkMaxValues();
    }

    /**
     * Method update values in maxEven and MaxOdd.
     */
    public final void checkMaxValues() {
        if (endRange % 2 == 0) {
            maxEven = endRange;
            maxOdd = endRange - 1;
        } else {
            maxOdd = endRange;
            maxEven = endRange - 1;
        }
    }

    /**
     * method prints odd numbers from start of fibonacci sequance.
     */
    public final void printOddNumbersFromStart() {
        System.out.println("Odd numbers from start: ");
        int sumNumbers = 0;
        int startPoint;
        if (startRange % 2 == 0) {
            startPoint = startRange + 1;
        } else {
            startPoint = startRange;
        }
        for (int i = startPoint;
             i <= endRange; i += 2) {

            sumNumbers += i;
            System.out.print(i + " ");
        }
        System.out.println("\nSum of odd numbers: " + sumNumbers);
    }

    /**
     * method prints even numbers from end of fibonacci sequance.
     */
    public final void printEvenNumbersFromEnd() {
        System.out.println("Even numbers from end: ");
        int sumNumbers = 0;
        int startPoint;
                if (endRange % 2 == 0) {
                    startPoint = endRange;
                } else {
                    startPoint = endRange - 1;
                }
        for (int i = startPoint; i >= startRange; i -= 2) {

            sumNumbers += i;
            System.out.print(i + " ");
        }
        System.out.println("\nSum of even numbers: " + sumNumbers);
    }

    /**
     *
     * @return max odd element from fibonacci sequance
     */
    public final int getMaxOdd() {
        return maxOdd;
    }

    /**
     *
     * @return max even element from fibonacci sequance
     */
    public final int getMaxEven() {
        return maxEven;
    }


}

